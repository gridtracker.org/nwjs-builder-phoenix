
import { test } from 'ava';

import { Downloader } from '../';

test('fetch', async (t) => {

    const mirror = process.env.CI ? undefined : 'https://dl.nwjs.io/';

    const downloader = new Downloader({
        platform: process.platform,
        arch: 'x64',
        version: '0.68.1',
        flavor: 'normal',
        mirror,
    });

    await downloader.fetch();

});
